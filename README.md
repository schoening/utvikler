## Zpirit utvikler +1 

**Er du klar for å utvikle fremtidens applikasjoner for kundene våre?**

**Vi har ledig stilling som front-/backendutvikler!**

Besøk [nettsiden](https://zpirit.no/) vår og les mer om hvem vi leter etter. 

Du bør ha god kunnskap om: 

* HTML 
* CSS 
* Javascript 
* PHP

Det er ønskelig at du har kunnskap om: 

* SASS/SCSS 
* Node
* Gulp/Grunt 
* Wordpress 
* Git
* JS ES6


### Slik søker du 

1. Besøk vår [Gitlab.com-profil](https://gitlab.com/zpirit) og åpne repo [utvikler](https://gitlab.com/zpirit/utvikler).

2. Fork vår repo.

3. Legg til din søknad i fila README.md

4. Legg til cv og eventuelt andre vedlegg.

5. Lag ein pull request til upstream repo master branch.


### Privat søknad?

Ønsker du å holde din cv og søknad privat kan du gjør følgende.

1. Opprett en privat repo på Gitlab, eventuelt på bitbucket eller github.
 
2. Gi lese tilgang til gruppen zpirit.

Vi har samme team navn på Bitbucket som på gitlab.

### Spørsmål

For spørsmål rundt søking kan du kontakte vårt utivkler team eller lese full beskrivelse på nettsiden vår.

### Kontakt

Espen Kvalheim <espen@zpirit.no>

Øyvind Eikeland <oyvind@zpirit.no>